<?php
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
  <head>
    <?php print $head ?>
    <title><?php print $head_title ?></title>
    <?php print $styles ?>
    <?php print $scripts ?>
  </head>
  <body>

<div id="main_container">

	<div id="header">
        <div class="logo">
            <?php
            // Prepare header
            if ($logo) {
            print '<h1><a href="'. check_url($front_page) .'" title="'. $site_title .'">';
            if ($logo) {
            print '<img src="'. check_url($logo) .'" alt="'. $site_title .'" id="logo" />';
            }
            print '</a></h1>';
            }
            ?>
        </div>

        <div id="menu_tab">
            <?php if (isset($primary_links)) : ?>
                <?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
            <?php endif; ?>
        </div>

        <div class="search_tab">
            <?php if (isset($search)) : ?>
                <?php print $search; ?>
            <?php endif; ?>
        </div>
    </div>




    <div id="main_content">
          <div class="left_sidebar">
                <div id="left_menu">
                    <?php if (isset($left)) : ?>
                        <?php print $left; ?>
                    <?php endif; ?>
                </div>

                <div class="submenu_pic">
                </div>
         </div>


        <div id="center_content">
            <?php print $breadcrumb; ?>
            <?php if ($mission): print '<div id="mission">'. $mission .'</div>'; endif; ?>
            <?php if ($tabs): print '<div id="tabs-wrapper" class="clear-block">'; endif; ?>
            <?php if ($title): print '<h2'. ($tabs ? ' class="with-tabs"' : '') .'>'. $title .'</h2>'; endif; ?>
            <?php if ($tabs): print '<ul class="tabs primary">'. $tabs .'</ul></div>'; endif; ?>
            <?php if ($tabs2): print '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>
            <?php if ($show_messages && $messages): print $messages; endif; ?>
            <?php print $help; ?>
            <?= $content ?>
        </div>

        <div class="clear"></div>

        <div id="footer">
            <div class="left_footer">
                <?php if (isset($left_footer)) : ?>
                    <?php print $left_footer; ?>
                <?php endif; ?>
            </div>
            <div class="center_footer">
                <?php if (isset($center_footer)) : ?>
                    <?php print $center_footer; ?>
                <?php endif; ?>

            </div>
            <div style="clear: both; width: 100%; text-align: center; font-size: 9px;"><a href="http://www.vip-consult.co.uk/website-design">Website Design By Vip-consult</a></div>
        </div>

    </div>    
  <?php print $closure ?>
</body>
</html>
